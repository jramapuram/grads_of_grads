import tensorflow as tf

# define a simple graph
x = tf.placeholder(tf.float32)
func0 = 3*x + 5
func1 = 3*x*x + 5

# define the gradients [can only take grads of
#                       scalar outputs [eg: sum / reduce, etc]]
grads_func0 = tf.gradients(func0, x)
grads_func1 = tf.gradients(func1, x)

# define grads of grads
# let's ignore grads_func0 for obvious reasons
grads_grads_func1 = tf.gradients(grads_func1, x)

# create a session and run the graph with x = 4
sess = tf.InteractiveSession()
feed_dict = {x: 4}
f0, f1, gf0, gf1, ggf1 = sess.run([func0, func1, grads_func0,
                                   grads_func1, grads_grads_func1],
                                  feed_dict=feed_dict)
print("3x + 5 = ", f0)
print("3x*x + 5 = ", f1)
print("d(3x + 5)/dx = ", gf0)
print("d(3x*x + 5)/dx = ", gf1)
print("d(d(3x*x + 5))/dx = ", ggf1)
